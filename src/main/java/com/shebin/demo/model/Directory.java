package com.shebin.demo.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Data
@Entity
@Table(name = "directory")
@XmlRootElement(name = "directory")
@JsonPropertyOrder({"id", "word"})
public class Directory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;    
    
    @Column(name = "word")
    private String word;
    
        
    @XmlElement
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}	
	
	@XmlElement
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}    
    
}
