package com.shebin.demo.model.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.shebin.demo.model.Directory;
import com.shebin.demo.service.DirectoryService;
@RestController
public class FileUploader {
	
	@Autowired
    private DirectoryService directoryService;
	
	@RequestMapping(value="/find/{word}", method = RequestMethod.GET, produces={MediaType.ALL_VALUE})
    public ResponseEntity<Boolean> getWord(HttpServletRequest request,@PathVariable String word){		
		Directory directory = directoryService.findUserByWord(word);
		if(directory != null){
			return new ResponseEntity<Boolean>(true,
	         		getHttpHeaders(request), HttpStatus.OK);
		}else{
			return new ResponseEntity<Boolean>(false,
	         		getHttpHeaders(request), HttpStatus.NOT_FOUND);
		}
        
    }
	 	 
   @PostMapping("/uploadFile")
    public ResponseEntity<String> uploadFile(HttpServletRequest request,@RequestParam("file") MultipartFile file) {  
        if (!file.isEmpty()) {	                         
                BufferedReader br;
                try {
                     String line;
                     InputStream is = file.getInputStream();
                     br = new BufferedReader(new InputStreamReader(is));
                     while ((line = br.readLine()) != null) {
                    	 String words[] = line.split("-|\\.");
                    	 if(words.length > 0){
                    		 for(int i =0; i< words.length ;i++){
	                    		 Directory directory = new Directory();
	                    		 directory.setWord(words[i]);
								 directoryService.saveUser(directory );
                    		 }
                    	 }
                     }
                     return new ResponseEntity<String>("You successfully uploaded ",
 	                		getHttpHeaders(request), HttpStatus.OK);
                  }catch (Exception e) {
  	                return new ResponseEntity<String>("You failed to upload " + e.getMessage(),
	                		getHttpHeaders(request), HttpStatus.NOT_IMPLEMENTED);
	            }
	        } else {
	            return new ResponseEntity<String>("You failed to upload ",
                		getHttpHeaders(request), HttpStatus.NOT_IMPLEMENTED);
	        }  	
	            	
	               
	           
	        
	        }  
	   
	   private HttpHeaders getHttpHeaders(HttpServletRequest request){
			String format = request.getContentType();
			HttpHeaders httpHeaders = new HttpHeaders();
			if(format == null || format.equals("")){
				format = "application/xml";
			}
		    if(format.equals("application/json")){
		        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		    }
		    else{
		        httpHeaders.setContentType(MediaType.APPLICATION_XML);
		    }	    
			return httpHeaders;
		}
		
}
