package com.shebin.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shebin.demo.model.Directory;

@Repository("directoryRepository")
public interface DirectoryRepository extends JpaRepository<Directory, Integer> {
    Directory findByWord(String word);

}