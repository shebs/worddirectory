package com.shebin.demo.service;

import com.shebin.demo.model.Directory;

public interface DirectoryService {
	
	Directory findUserByWord(String word);
	
	void saveUser(Directory directory);
	

}
