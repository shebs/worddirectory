package com.shebin.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shebin.demo.model.Directory;
import com.shebin.demo.repository.DirectoryRepository;

@Service("directoryServiceImpl")
public class DirectoryServiceImpl implements DirectoryService{

	    private DirectoryRepository directoryRepository;
	    
	    @Autowired
	    public DirectoryServiceImpl(DirectoryRepository directoryRepository) {
	        this.directoryRepository = directoryRepository;	        
	    }

	    public Directory findUserByWord(String word) {
	        return directoryRepository.findByWord(word);
	    }

	    public void saveUser(Directory directory) {	    	       
	        directoryRepository.save(directory);
	    }	

}
